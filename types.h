#include <list>

struct process{
	int pID;
	int arrivalTime;
	int lifeTime;
	int numOfPieces;
	int* pieces;
};

struct slot{
	int slotNum;
	int filledUntilTime;
	int filledByProcess;
    int pageNumber;
	bool isAvailable;
};

class MemManager{
public:
	MemManager(int memSize, int pageSize, int numOfProcesses);
    int averageTurnAroundTime();
	void insert(process p, int time);
	void anythingFinished(int time);
	void anythingWaiting(int time);
    bool allCompleted();
	void output(int time,int eventNum, int processNum);
	~MemManager();
private:
	int _numOfSlots;
    int _numOfProcesses;
	int _pageSize;
	int _memSize;
    int _completed;
	slot** _slots;
	slot* makeEmptySlot();
	void moveToSlot(slot* s,process p, int time, int pageNum);
	void emptySlot(slot* s);
	int pagesNeeded(process p, int pageSize);
	int slotsAvailable();
	std::list<process> _waiting;
    int* _taTime;
	int firstAvailableSlot();

};

