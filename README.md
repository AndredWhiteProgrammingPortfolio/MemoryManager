<snippet>
<content>
# Memory Manager
Project that simulates the effects of limited memory and memory management policies.
## Installation
From project folder make from terminal
## Usage
Run make to compile and ./memManager to run
The program accepts arguments in order to bypass the user input. Possible Options are:
./memManager (memSize) (1|2|3) [filename] where memSize is the size of the memory chunk, page size corresponds to 1:100 2:200 3:400. and filename is the name of the workload file. 

## Credits
CPSC 351 Professor Yun Tian
## License
Copyright 2016 Andre White
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</snippet>
</content>
