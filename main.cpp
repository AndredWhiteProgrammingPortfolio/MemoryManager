/*
    Andre White
    CPSC 351
    Assignment 3
 */

# include "types.h"
#include <fstream>
#include <iostream>
#include <stdlib.h>
using namespace std;
int numOfProcesses; 
process** getProcesses(string filename){
	
	process** processes;	
	ifstream fin;
	fin.open(filename.c_str());
	if(fin.is_open()){
		fin>>numOfProcesses;
		processes= new process* [numOfProcesses];
		for(int x=0;x<numOfProcesses;x++){
			process* p= new process;
			fin>>p->pID;
			fin>>p->arrivalTime;
			fin>>p->lifeTime;
			fin>>p->numOfPieces;
			p->pieces= new int[p->numOfPieces];
			for(int y=0;y<p->numOfPieces;y++){
				fin>>p->pieces[y];
			}
			processes[x]=p;
		}
	}
    else{
        cout<<"error opening file"<<endl;
        exit(1);
    }
	fin.close();
	return processes;
}
int memSize;
int pageSize;
string filename;
void usage(){
    cout<<"./run [memSize] [pageSize 1|2|3] [filename]"<<endl;
    cout<<"./run [memSize] [pageSize 1|2|3]"<<endl;
    cout<<"Where  Page Size (1:100,2:200,3:400)"<<endl;
    exit(1);
    
}
int main(int argc, char** argv){
    bool filenamefilled=false;
    if (argc>1) {
        if (argc==3) {
            memSize=atoi(argv[1]);
            pageSize=atoi(argv[2]);
        }
        else if (argc==4) {
            memSize=atoi(argv[1]);
            pageSize=atoi(argv[2]);
            filename=argv[3];
            filenamefilled=true;
        }
        else{
            usage();
        }
    }
    else{
        cout<<"Memory Size>";
        cin>>memSize;
        cout<<"Page Size (1:100,2:200,3:400)>";
        cin>>pageSize;
    }
	
	
	int clock=0;	
	
	switch(pageSize){
		case 1:
			pageSize=100;
			break;
		case 2:
			pageSize=200;
			break;
		case 3:
			pageSize=400;
			break;
        default:
            cout<<"Please select only supported page size!!!"<<endl;
            exit(1);
	}
    if (!filenamefilled) {
        cout<<"Enter Workload Filename"<<endl;
        cin>>filename;
        
    }
	process** processes=getProcesses(filename);
    if (processes==NULL) {
        cout<<"Error Loading Processes"<<endl;
        exit(1);
    }
	list<process*> things;
	for(int x=0;x<numOfProcesses;x++){
		things.push_back(processes[x]);
	}
	MemManager mem=MemManager(memSize,pageSize, numOfProcesses);
	while(clock<10000&&!mem.allCompleted()){
		if(clock!=0){
			mem.anythingFinished(clock);
		}
		while(!things.empty()&&clock==things.front()->arrivalTime){
			mem.insert(*things.front(),clock);
			things.pop_front();
		}
		mem.anythingWaiting(clock);		
	clock++;
	}
    printf("Average turnaround time is: %d \n", mem.averageTurnAroundTime());
	

return 0;
}
