#created by: Andre White 2017
#compiler: g++
all: memManager

memManager: types.o main.cpp
	g++ types.o main.cpp -o memManager

types.o: types.cpp
	g++ -c types.h types.cpp
clean:
	rm *o memManager
