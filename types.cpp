#include "types.h"
#include <stdio.h>
MemManager::MemManager(int memSize, int pageSize, int numOfProcesses){
	_memSize=memSize;
	_pageSize=pageSize;
	_numOfSlots=memSize/pageSize;
    _numOfProcesses=numOfProcesses;
    _completed=0;
    _taTime=new int[_numOfProcesses];
	_slots=new slot*[_numOfSlots];
	for(int x=0; x<_numOfSlots; x++)
	{
		slot* s=makeEmptySlot();
		s->slotNum=x;
		_slots[x]=s;
	}
}
slot* MemManager::makeEmptySlot(){
	slot* s= new slot;
	s->isAvailable=true;
	s->slotNum=-1;
	s->filledUntilTime=-1;
	s->filledByProcess=-1;
    s->pageNumber=-1;
	return s;
}
int MemManager::averageTurnAroundTime(){
    int total=0;
    for (int x=0; x<_numOfProcesses; x++) {
        total+=_taTime[x];
    }
    return(total/_numOfProcesses);
}
int MemManager::firstAvailableSlot(){
	int availableSlot=0;
	for(int x=0; x<_numOfSlots; x++){
		if(_slots[x]->isAvailable==true){
			availableSlot=x;
			break;
		}
	}
	return availableSlot;

}
int MemManager::pagesNeeded(process p,int pageSize){

	int storage=0;
	int nop= p.numOfPieces;
	for(int x=0; x<nop; x++){
		storage+=p.pieces[x];
	}
	if(storage%pageSize!=0)
		return (storage/pageSize)+1;
	else
		return(storage/pageSize);
}
int MemManager::slotsAvailable(){
	int numFree=0;	
	for(int x=0; x<_numOfSlots; x++)
	{
		if(_slots[x]->isAvailable)
			numFree++;
	}
	return numFree;
}
void MemManager::emptySlot(slot* s){
	s->filledUntilTime=-1;
	s->filledByProcess=-1;
	s->isAvailable=true;
    s->pageNumber=-1;
}
bool MemManager::allCompleted(){
    if(_completed==_numOfProcesses)
        return true;
    else
        return false;
}
void MemManager::anythingFinished(int time){
	for(int x=0; x<_numOfSlots; x++){
		if(_slots[x]->filledUntilTime==time){
			int processNum= _slots[x]->filledByProcess;
			
			for(int y=0;y<_numOfSlots;y++)
			{
				if(_slots[y]->filledByProcess==processNum){
					emptySlot(_slots[y]);
				}
			}
            _completed+=1;
			output(time, 2, processNum);
		}
	}
	anythingWaiting(time);
}
void MemManager::insert(process p, int time){
	_waiting.push_back(p);
	output(time, 0, p.pID);
}

void MemManager::anythingWaiting(int time){
	if(!_waiting.empty()){
        for(std::list<process>::iterator it=_waiting.begin(); it!=_waiting.end();++it) {
            process p=*it;
            if(p.arrivalTime<=time){
                if(pagesNeeded(p,_pageSize)<=slotsAvailable()){
                    for(int y=0;y<pagesNeeded(p,_pageSize);y++){
                        int x=firstAvailableSlot();
                        if(x!=-1){
                            moveToSlot(_slots[x], p, time, y+1);
                        }
                    }
                    _waiting.erase(it);
                    output(time, 1, p.pID);
					
                }
            }
        }
    }
}
void MemManager::moveToSlot(slot* s,process p, int time, int pageNum){
	s->isAvailable=false;
	s->filledByProcess=p.pID;
	s->filledUntilTime=time+p.lifeTime;
    s->pageNumber=pageNum;
    _taTime[p.pID-1]=s->filledUntilTime-p.arrivalTime;
}
MemManager::~MemManager(){
	for(int x=0;x<_numOfSlots;x++){
		delete(_slots[x]);
	}
}
void MemManager::output(int time, int eventNum, int processNum){
	switch(eventNum){
		case 0:
			printf("t=%d: Process %d arrives\n", time, processNum);
			break;
		case 1:
			printf("t=%d: MM moves process %d to memory\n", time, processNum);
			break;
		case 2:
			printf("t=%d: Process %d completes\n", time, processNum);
			break;
	}
	printf("%10s","Input Queue:[");
	for(std::list<process>::iterator it=_waiting.begin(); it!=_waiting.end();++it){
		process p=*it;		
		printf("%d ", p.pID);
	}
	printf("]\n");
	printf("%10s","Memory Map:\n");
	int bottomRange=-1;
	int topRange=0;
	bool freeframes=false;
	for(int x=0;x<_numOfSlots;x++){
		if(_slots[x]->isAvailable==true){
			if(bottomRange==-1){		
				bottomRange=_pageSize*x;
			}
		topRange=(_pageSize*(x+1))-1;
		freeframes=true;
		}
		else if(_slots[x]->isAvailable==false){
			if(freeframes==true){
				printf("%10d-%d: Free frame(s)\n",bottomRange, topRange);
				freeframes=false;
			}
			bottomRange=_pageSize*x;
			topRange=(_pageSize*(x+1))-1;
			printf("%10d-%d: Process %d, Page %d\n",bottomRange, topRange, _slots[x]->filledByProcess, _slots[x]->pageNumber);
			bottomRange=-1;
		}
	}
	if(freeframes==true){
		printf("%10d-%d: Free frame(s)\n",bottomRange, topRange);
		freeframes=false;
	}
		
}
	
